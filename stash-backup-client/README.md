# Stash Backup Client Docker image

See https://confluence.atlassian.com/display/STASH/Using+the+Stash+Backup+Client for documentation on the backup client

A simple script has been included that takes environment variables and adds them as defines to the java command. So running a backup could look like this:

    docker run --rm \
      --env STASH_USER=admin \
      --env STASH_PASSWORD=admin \
      --env STASH_BASE_URI=<stash url> \
      --volumes-from=<stash-data-container> \
      -v <place to store backup>:/opt/backup \
      kristoffer/atlassian-stash-backup-client
  